package ru.malakhov.tm.model;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
