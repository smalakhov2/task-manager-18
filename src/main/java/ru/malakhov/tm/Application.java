package ru.malakhov.tm;

import ru.malakhov.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}