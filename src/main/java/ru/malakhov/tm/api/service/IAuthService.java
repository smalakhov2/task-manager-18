package ru.malakhov.tm.api.service;

import ru.malakhov.tm.model.Role;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void logout();

    void checkRole(Role[] roles);

}