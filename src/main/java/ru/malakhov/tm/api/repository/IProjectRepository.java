package ru.malakhov.tm.api.repository;

import ru.malakhov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    List<Project> findAll(String userId);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneById(String userId, String id);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

    Project merge(Project project);

    void merge(Project... projects);

    void merge(Collection<Project> projects);

    void load(Project... projects);

    void load(Collection<Project> projects);

    void clear();

    List<Project> getProjectList();

}