package ru.malakhov.tm.service;

import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    public DomainService(final ITaskService taskService, final IProjectService projectService, final IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        projectService.load();
        taskService.load();
        userService.load();
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getProjectList());
        domain.setTasks(taskService.getTaskList());
        domain.setUsers(userService.getUserList());
    }

}