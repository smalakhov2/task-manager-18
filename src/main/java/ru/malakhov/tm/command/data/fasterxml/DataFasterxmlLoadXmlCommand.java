package ru.malakhov.tm.command.data.fasterxml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterxmlLoadXmlCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-fasterxml-xml-load";
    }

    @Override
    public String description() {
        return "Load xml fasterxml date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML XML LOAD]");
        final String xml = new String(Files.readAllBytes(Paths.get(DataConst.DATA_FASTERXML_XML_PATH)));

        final XmlMapper xmlMapper = new XmlMapper();
        Domain domain = xmlMapper.readValue(xml, Domain.class);

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
    }

}