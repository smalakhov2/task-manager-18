package ru.malakhov.tm.command.data.base64;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import static ru.malakhov.tm.constant.DataConst.BINARY_PATH;

public class DataBase64LoadCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "db64-load";
    }

    @Override
    public String description() {
        return "Load base64 date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final String base64 = Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(BINARY_PATH)));
        final byte[] decBase64 = new BASE64Decoder().decodeBuffer(base64);

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decBase64);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

        byteArrayInputStream.close();
        objectInputStream.close();
        System.out.println("[OK]");

    }

}