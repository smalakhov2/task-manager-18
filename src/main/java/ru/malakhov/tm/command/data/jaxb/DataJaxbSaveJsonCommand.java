package ru.malakhov.tm.command.data.jaxb;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataJaxbSaveJsonCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-jaxb-json-save";
    }

    @Override
    public String description() {
        return "Save json jaxb data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConst.DATA_JAXB_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);

        System.out.println("[OK]");
    }

}