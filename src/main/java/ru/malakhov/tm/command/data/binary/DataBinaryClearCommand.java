package ru.malakhov.tm.command.data.binary;

import ru.malakhov.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.DataConst.BINARY_PATH;

public class DataBinaryClearCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "binary-clear";
    }

    @Override
    public String description() {
        return "Clear binary data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY CLEAR]");
        final File file = new File(BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

}