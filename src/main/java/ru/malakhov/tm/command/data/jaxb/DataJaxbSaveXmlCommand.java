package ru.malakhov.tm.command.data.jaxb;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataJaxbSaveXmlCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-jaxb-xml-save";
    }

    @Override
    public String description() {
        return "Save xml jaxb data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConst.DATA_JAXB_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, file);

        System.out.println("[OK]");
    }

}