package ru.malakhov.tm.command.data.jaxb;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;

import java.io.File;
import java.nio.file.Files;

public class DataJaxbClearJsonCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-jaxb-json-clear";
    }

    @Override
    public String description() {
        return "Clear json jaxb data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON CLEAR]");
        final File file = new File(DataConst.DATA_JAXB_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

}