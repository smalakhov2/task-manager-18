package ru.malakhov.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataFasterxmlSaveJsonCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-fasterxml-json-save";
    }

    @Override
    public String description() {
        return "Save json fasterxml data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML JSON SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final File file = new File(DataConst.DATA_FASTERXML_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(DataConst.DATA_FASTERXML_JSON_PATH);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}