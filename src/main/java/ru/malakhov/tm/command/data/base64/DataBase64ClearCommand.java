package ru.malakhov.tm.command.data.base64;

import ru.malakhov.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.DataConst.BASE64_PATH;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "db64-clear";
    }

    @Override
    public String description() {
        return "Clear base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        final File file = new File(BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");

    }

}