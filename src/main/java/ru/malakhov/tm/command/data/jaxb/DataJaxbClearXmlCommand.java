package ru.malakhov.tm.command.data.jaxb;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;

import java.io.File;
import java.nio.file.Files;

public class DataJaxbClearXmlCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-jaxb-xml-clear";
    }

    @Override
    public String description() {
        return "Clear xml jaxb data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML CLEAR]");
        final File file = new File(DataConst.DATA_JAXB_XML_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

}