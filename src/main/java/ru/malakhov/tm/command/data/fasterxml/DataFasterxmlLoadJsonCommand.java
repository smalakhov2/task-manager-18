package ru.malakhov.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterxmlLoadJsonCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-fasterxml-json-load";
    }

    @Override
    public String description() {
        return "Load json fasterxml date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML JSON LOAD]");
        final String json = new String(Files.readAllBytes(Paths.get(DataConst.DATA_FASTERXML_JSON_PATH)));

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
        final Domain domain = objectMapper.readValue(json, Domain.class);

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
    }

}