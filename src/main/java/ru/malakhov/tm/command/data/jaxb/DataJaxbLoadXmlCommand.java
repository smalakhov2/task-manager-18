package ru.malakhov.tm.command.data.jaxb;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import java.io.File;

public class DataJaxbLoadXmlCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-jaxb-xml-load";
    }

    @Override
    public String description() {
        return "Load xml jaxb date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML LOAD]");
        final File file = new File(DataConst.DATA_JAXB_XML_PATH);

        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Domain domain = (Domain) jaxbContext.createUnmarshaller().unmarshal(file);

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
    }

}