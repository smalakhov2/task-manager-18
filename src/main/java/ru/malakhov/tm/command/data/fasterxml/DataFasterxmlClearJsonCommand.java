package ru.malakhov.tm.command.data.fasterxml;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;

import java.io.File;
import java.nio.file.Files;

public class DataFasterxmlClearJsonCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-fasterxml-json-clear";
    }

    @Override
    public String description() {
        return "Clear json fasterxml data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML JSON CLEAR]");
        final File file = new File(DataConst.DATA_FASTERXML_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

}