package ru.malakhov.tm.command.data.jaxb;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxbLoadJsonCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "data-jaxb-json-load";
    }

    @Override
    public String description() {
        return "Load json jaxb date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON LOAD]");
        final File file = new File(DataConst.DATA_JAXB_JSON_PATH);

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        final Domain domain = (Domain) unmarshaller.unmarshal(file);

        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());

        System.out.println("[OK]");
    }

}